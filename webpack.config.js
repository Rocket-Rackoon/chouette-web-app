const path                = require ('path'),
      autoprefixer        = require ('autoprefixer'),
      webpack             = require ("webpack"),
      libPath             = path.join (__dirname, 'src'),
      wwwPath             = path.join (__dirname, 'dist'),
      pkg                 = require ('./package.json'),
      HtmlWebpackPlugin   = require ('html-webpack-plugin'),
      UglifyJSPlugin      = require ('uglifyjs-webpack-plugin'),
      DashboardPlugin     = require ('webpack-dashboard/plugin'),
      Chouette            = require ('./webpack/factory.js'),
      DocsGeneratorPlugin = require ('webpack-angular-dgeni-plugin'),
      ExtractTextPlugin   = require ("extract-text-webpack-plugin");


let config = {
  entry    : path.join (libPath, 'index.js'),
  devtool  : ("production" === process.env.NODE_ENV) ? "source-map" : "eval-source-map",
  output   : {
    path    : path.join (wwwPath),
    filename: 'js/chouette-bundle.js'
  },
  module   : {
    rules: [
      {
        enforce: "pre",
        test   : /\.js$/,
        exclude: /node_modules/,
        loader : "eslint-loader",
      },
      {
        test   : /\.js$/,
        loaders: ['babel-loader'],
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use : ExtractTextPlugin.extract ({
          fallback: "style-loader",
          use     : ['css-loader', 'sass-loader'],
        })
      },
      {
        test  : /\.tpl\.html$/i,
        loader: 'file-loader?name=templates/[name].[ext]'
      },
      {
        test   : /\.(gif|png|jpe?g|svg)$/i,
        loaders: [
          'file-loader?name=img/[name].[ext]',
          {
            loader: 'image-webpack-loader',
            query : {
              mozjpeg : {
                progressive: true,
              },
              gifsicle: {
                interlaced: true,
              },
              optipng : {
                optimizationLevel: 7,
              }
            }
          }
        ]
      },
      { test: /\.woff$/, loader: 'url-loader?limit=65000&mimetype=application/font-woff&name=fonts/[name].[ext]' },
      { test: /\.woff2$/, loader: 'url-loader?limit=65000&mimetype=application/font-woff2&name=fonts/[name].[ext]' },
      { test: /\.[ot]tf$/, loader: 'url-loader?limit=65000&mimetype=application/octet-stream&name=fonts/[name].[ext]' },
      {
        test  : /\.eot$/,
        loader: 'url-loader?limit=65000&mimetype=application/vnd.ms-fontobject&name=fonts/[name].[ext]'
      }
    ]
  },
  resolve  : {
    alias: {
      modules   : path.join (__dirname, 'src/modules'),
      components: path.join (__dirname, 'src/components'),
      libs    : path.join (__dirname, 'src/libs'),
      config    : path.join (__dirname, 'config'),
    }
  },
  externals: {
    angular: 'angular'
  },
  plugins  : [
    new DashboardPlugin ({ port: 3001 }),
    new ExtractTextPlugin ('css/style.css'),
    new Chouette.plugin.translation ({
      languages: ['en', 'fr'],
      output   : 'locales'
    }),
    new webpack.DefinePlugin ({
      'process.env.NODE_ENV': JSON.stringify (process.env.NODE_ENV)
    }),
    new HtmlWebpackPlugin ({
      filename: 'index.html',
      pkg     : pkg,
      inject  : 'body',
      template: path.join (libPath, 'public', 'index.html')
    }),
  ]
};

if (process.env.NODE_ENV === 'production') {
  config.plugins.push (
    new UglifyJSPlugin ({
      sourceMap: true,
      compress : {
        warnings: true
      }
    })
  );
  config.plugins.push (
    new webpack.LoaderOptionsPlugin ({
      minimize: true
    })
  );
  config.push (
    new DocsGeneratorPlugin ({
      enable       : true,
      staticContent: './docs',
      sources      : {
        include : 'src/app/**/*.js',
        basePath: 'src/app'
      },
      output       : 'dist-docs'
    })
  );
} else {
  config.plugins.push (
    new webpack.LoaderOptionsPlugin ({
      debug: true
    })
  );
}

module.exports = config;
