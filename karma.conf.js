const webpackConfig = require ('./webpack.config');

// Karma configuration
module.exports = function (config) {
  config.set ({

    frameworks: ['mocha', 'sinon-chai'],

    client: {
      mocha         : {
        'ui': 'tdd'
      },
      chai          : {
        includeStack: true
      },
      captureConsole: false
    },

    // ... normal karma configuration
    files: [
      // all files ending in "_test"
      'test/unit/test_index.js'
      // each file acts as entry point for the webpack configuration
    ],

    preprocessors: {
      // add webpack as preprocessor
      'test/unit/test_index.js': ['webpack', 'sourcemap', 'coverage']
    },

    webpack: webpackConfig,

    plugins: [
      'karma-mocha',
      'karma-webpack',
      'karma-coverage',
      'karma-sinon-chai',
      'karma-sourcemap-loader',
      'karma-phantomjs2-launcher'
    ],

    webpackMiddleware: {
      // webpack-dev-middleware configuration
      // i. e.
      stats: 'errors-only'
    },

    // tests results reporter to use
    // possible values: 'dots', 'progress', 'mocha'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['dots', 'progress', 'coverage'],

    mochaReporter: {
      showDiff: true,
    },

    coverageReporter: {
      dir      : 'ci',
      reporters: [
        {
          type  : 'html',
          subdir: 'coverage'
        }
      ]
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS2'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true

  });
};
