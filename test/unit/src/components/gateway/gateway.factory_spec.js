import angular          from 'angular';
import ngMock           from 'angular-mocks';
import Router           from 'angular-ui-router';
import {sinon}          from 'sinon';

import GatewayModule    from 'components/gateway/gateway.module';
import WebStoreModule   from 'components/webstorage/webstorage.module';
import GatewayFactory   from 'components/gateway/gateway.factory';
import ToasterModule    from 'components/toaster/toaster.module';


describe('Module Gateway', () => {

    describe('Factory GatewayService', function () {

        let GatewayService, $httpBackend, url, tokens, url_full, $toaster, $window;

        beforeEach(angular.mock.module(Router));
        beforeEach(angular.mock.module(GatewayModule));
        beforeEach(angular.mock.module(ToasterModule));

        beforeEach(() => {

            // Generate fake token
            //tokens = oauthMock();

            //stubbing window location
            $window = {location: { href: sinon.spy()} };
            angular.mock.module(function($provide) {
                $provide.value('$window', $window);
            });

            angular.module(GatewayModule)
                .config((GatewayAPIConfigProvider) => {
                    GatewayAPIConfigProvider.configure(require('config/parameters.json'));
                });
        });


        beforeEach(
            inject(function (_$gateway_, _$httpBackend_, _GatewayAPIConfig_, _$toaster_, _WebStorageFactory_) {

                $toaster = _$toaster_;
                GatewayService = _$gateway_;
                $httpBackend = _$httpBackend_;
                let provider = _GatewayAPIConfig_;
                url      = 'user?access_token=1234';
                url_full = `${provider.$getConf.api.url}user?access_token=1234`;
                _WebStorageFactory_.set('user_company', {
                    slug: 'toto'
                })
            })
        );

        it('should have service factory Gateway', function () {
            assert.isNotNull((GatewayService));
        });

        it('should given throw error when there are nothing', function () {

            assert.throws(
                function () {
                    GatewayService.fetch();
                },
                'You need to write url property in your object.'
            );


            assert.throws(
                function () {
                    GatewayService.fetch('');
                },
                'You need to write url property in your object.'
            );

        });
        it('should valid with good given options data', function () {

            let promise = GatewayService
                .fetch(url);

            $httpBackend.when("GET", url_full).respond(200, 'hello');
            promise.then((response) => {
                console.log('response', response);
                assert.isNotNull(response);
                assert.equal('GET', response.config.method);
                assert.equal('hello', response.data);
            });

            $httpBackend.flush();
        });
        it('should valid call url with a good token', function () {

            $httpBackend.when("GET", url_full).respond(200, 'hello');

            let data = GatewayService.fetch(url);
            data.then(function (response) {
                //assert.include(response.config.url, tokens.access);
                assert.equal(response.config.method, 'GET');

            });

            $httpBackend.flush();
        });
        it('should catch an error from the gateway service', function () {

            $httpBackend.when("GET", url_full).respond(400, 'Form unexpected');

            let promise = GatewayService
                .fetch(url);
            promise.then(
                () => {
                },
                (error) => {

                    assert.isNotNull(error);
                    assert.equal(error.status, 400);
                    assert.equal('Form unexpected', error.data);
                }
            );

            $httpBackend.flush();
        });
        it('should create or update a resource', function () {
            $httpBackend.when("POST", url_full).respond(400, 'Form unexpected');

            let promisePOST = GatewayService.push({
                url: url,
                method: 'POST',
                data: {}
            });

            promisePOST.then(
                () => {
                },
                (error) => {

                    assert.isNotNull(error);
                    assert.equal(error.status, 400);
                    assert.equal(error.config.method, 'POST');
                    assert.equal('Form unexpected', error.data);
                }
            );

            $httpBackend.when("PUT", url_full).respond(200, 'Ok', {});

            let promisePUT = GatewayService.push({
                url: url,
                method: 'PUT',
                data: {}
            });

            promisePUT.then(
                () => {
                },
                (error) => {

                    assert.isNotNull(error);
                    assert.equal(error.status, 400);
                    assert.equal(error.config.method, 'PUT');
                    assert.equal('Form unexpected', error.data);
                }
            );

            $httpBackend.flush();
        });
        it('should remove a resource with a bad token', function () {
            $httpBackend.when("DELETE", url_full).respond(403, 'Not authorize');

            let promiseDelete = GatewayService.remove(url);

            promiseDelete.then(
                () => {
                },
                (error) => {

                    assert.isNotNull(error);
                    assert.equal(error.status, 403);
                    assert.equal(error.config.method, 'DELETE');
                    assert.equal('Not authorize', error.data);
                }
            );

            $httpBackend.flush();
        });
        it('should send a query to resource', function () {
            $httpBackend.when("DELETE", url_full).respond(403, 'Not authorize');

            let promiseDelete = GatewayService.query({
                url: url,
                method: 'DELETE'
            }, 'api');

            promiseDelete.then(
                () => {
                },
                (error) => {

                    assert.isNotNull(error);
                    assert.equal(error.status, 403);
                    assert.equal(error.config.method, 'DELETE');
                    assert.equal('Not authorize', error.data);
                }
            );

            $httpBackend.flush();
        });


    });
});
