import angular          from 'angular';
import ngMock           from 'angular-mocks';
import ngMD             from 'angular-material';
import GatewayModule    from 'libs/components/gateway/gateway.module';
import GatewayProvider    from 'libs/components/gateway/gateway.provider';


describe('Module Gateway', () => {

    describe('Provider GatewayAPIConfig', function () {


        let GatewayService, $httpBackend, url;

        let _configuration = {
            domain: {
                url: "http://platform.display.powerstaging.net:9000/v2/",
                version: 3.0
            },
            api: {
                url: "http://api-play.display.powerstaging.net:9000/v2/",
                version: 2.0
            },
            oauth: {
                url: "http://api.display.powerstaging.net/oauth/2.0/",
                version: 2.0
            },
            elasticSearch: {
                url: "http://stg-cloud-1.powerspace.com:9200/",
                version: 1.7
            }
        };

        beforeEach(angular.mock.module(ngMD));
        beforeEach(angular.mock.module(GatewayModule));
        beforeEach(() => {

            angular.module('stubs.gateway', [GatewayModule])
                .config(function (GatewayAPIConfigProvider) {
                    GatewayAPIConfigProvider.configure(_configuration);
                });

            angular.mock.module('stubs.gateway')
        });
        beforeEach(inject(function (_GatewayAPIConfig_, _$httpBackend_) {

            GatewayService = _GatewayAPIConfig_;
            $httpBackend = _$httpBackend_;
            url = 'http://localhost:8080/api/v1/test.html';
        }));


        it('should have provider factory GatewayAPIConfig', inject(function (GatewayAPIConfig) {

            assert.throws(function() {
                GatewayProvider()
            }, 'Cannot call a class as a function');
            assert.isDefined(GatewayAPIConfig);
            assert.isObject(GatewayAPIConfig.$getConf);
            assert.property(GatewayAPIConfig.$getConf, 'api');
            assert.property(GatewayAPIConfig.$getConf, 'oauth');
            assert.equal(GatewayAPIConfig.$getConf.api['version'], 2.0);
        }));
    });
});
