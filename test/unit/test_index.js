// test/test_index.js

// require all modules ending in "_test" from the
// current directory and all subdirectories
const testsContext = require.context(".", true, /_spec.js$/);
testsContext.keys().forEach(testsContext);
