import DropDownModule       from './dropdown/dropdown.module';
import GatewayModule        from './gateway/gateway.module';
import LanguageModule       from './language/language.module';
import LocalStorageModule   from './localstorage/localstorage.module';
import MenuModule           from './menu/menu.module';
import MapsModule           from './maps/maps.module';
import ToasterModule        from './toaster/toaster.module';

export default angular
    .module ('chouette.components', [
        DropDownModule,
        MenuModule,
        MapsModule,
        ToasterModule,
        GatewayModule,
        LanguageModule,
        LocalStorageModule
    ])
    .name;
