import 'angular-translate';
import 'angular-translate/dist/angular-translate-loader-url/angular-translate-loader-url';

import LanguageConfig from './language.config.js';

export default angular
    .module('chouette.language', [
        'pascalprecht.translate'
    ])
    .config(LanguageConfig)
    .name;
