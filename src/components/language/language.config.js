
function TranslateConfig ($translateProvider) {

    //TODO : files conf languages and forEach here

    let url = 'locales/fr.json';

    $translateProvider
        .useUrlLoader(url)
        .preferredLanguage('fr')
        .useSanitizeValueStrategy('sanitize');
}

TranslateConfig.$inject = ['$translateProvider'];

export default TranslateConfig;
