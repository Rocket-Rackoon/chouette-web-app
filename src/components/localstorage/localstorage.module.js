import ngStorage            from 'angular-local-storage';

import LocalStorageConfig   from './localstorage.config.js';
import LocalStorageFactory  from './localstorage.service';

export default angular
    .module('chouette.localstorage', [
        ngStorage
    ])
    .config(LocalStorageConfig)
    .factory('LocalStorage', LocalStorageFactory)
    .name;
