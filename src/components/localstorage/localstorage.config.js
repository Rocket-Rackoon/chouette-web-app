
function LocalStorageConfig (localStorageServiceProvider) {

    localStorageServiceProvider
        .setPrefix('fr.chouette')
        .setDefaultToCookie(true)
        .setStorageType('localStorage')
        .setStorageCookie(45, '/', false)
}

LocalStorageConfig.$inject = ['localStorageServiceProvider'];

export default LocalStorageConfig;
