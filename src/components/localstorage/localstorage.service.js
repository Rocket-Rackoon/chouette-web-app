/**
 * Store Gateway Factory
 */
class LocalStorageFactory {


    /**
     * Init Factory with dependency injection
     */
    static factory(localStorageService) {
        return new LocalStorageFactory(localStorageService);
    }

    /**
     * Constructor
     *
     * @param localStorageService
     */
    constructor(localStorageService) {

        let vm = this;
        vm.length = length();
        vm.get = getItem;
        vm.set = setItem;
        vm.clear = clear;
        vm.remove = removeItem;


        function length() {
            if (localStorageService.isSupported) {
                return localStorageService.keys().length;
            } else {
                return localStorageService.cookie.keys().length;
            }
        }

        function getItem(key) {
            if (localStorageService.isSupported) {
                return localStorageService.get(key);
            } else {
                return localStorageService.cookie.get(key);
            }
        }

        function setItem(key, value) {
            if (localStorageService.isSupported) {
                localStorageService.set(key, value);
            } else {
                localStorageService.cookie.set(key, value);
            }
        }

        function removeItem(key) {
            if (localStorageService.isSupported) {
                localStorageService.remove(key);
            } else {
                localStorageService.cookie.remove(key);
            }
        }

        function clear () {
            if (localStorageService.isSupported) {
                localStorageService.clearAll();
            } else {
                localStorageService.cookie.clearAll();
            }
        }
    }
}

LocalStorageFactory.factory.$inject = ['localStorageService'];

export default LocalStorageFactory.factory;
