import 'leaflet';
import 'leaflet-easyprint';
// import 'leaflet.gridlayer.googlemutant';

import 'angular-simple-logger';
import 'ui-leaflet';
import 'ui-leaflet-layers';

export default angular
    .module ('chouette.maps', [
        'nemLogging',
        'ui-leaflet'
    ])
    .name;
