class ToasterConfig {

    /**
     * configure the toaster
     * @param $httpProvider
     */
    static configure($httpProvider) {
        $httpProvider.interceptors.push('ToasterInterceptor');
    }
}

ToasterConfig.configure.$inject = ['$httpProvider'];

export default ToasterConfig.configure;
