export let toasterOptionsDefault = {
    'duration': 6000,
    'close': true
};

export let toasterMessagesInterceptor = {
    'PUT' : 'updated',
    'POST' : 'created',
    'DELETE' : 'deleted'
};
