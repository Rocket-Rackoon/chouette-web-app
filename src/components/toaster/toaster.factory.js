import { toasterMessagesInterceptor } from './toaster.config';

/**
 * Toaster Factory
 */
class ToasterFactory {

    /**
     *
     * @param $q
     * @param $injector
     * @return {ToasterFactory}
     */
    static factory($q, $injector) {
        return new ToasterFactory ($q, $injector);
    }

    constructor($q, $injector) {

        let vm = this;

        vm.responseError = responseError;
        vm.response      = response;

        function response(responseIntercepted) {

            if ((responseIntercepted.status === 200 || responseIntercepted.status === 201) && responseIntercepted.config.method !== 'GET') {

                let message = {};

                message.description = `Data has been successfully ${toasterMessagesInterceptor[responseIntercepted.config.method]}.`;
                message.type        = 'success';
                $injector.get('ngNotify').set(message.description, message.type);

            }

            return $q.resolve (responseIntercepted);
        }

        function responseError(error) {

            let message = {};
            if (error.status === 404) {
                message.description = `La page demandée n'existe pas.`;
            } else if (error.status === 401) {
                message.description = `Vous n'avez pas accès a cette page.`;
            } else if (error.status === 403) {
                message.description = `The entity that you're looking for was not found.`;
            } else {
                message.description = `Something wrong happened, please contact the support.`;
            }

            $injector.get('ngNotify').set(message.description, 'error');

            return $q.reject (error);
        }
    }
}

ToasterFactory.factory.$inject = ['$q', '$injector'];

export default ToasterFactory.factory;
