import 'ng-notify';

import './toaster.scss';
import ToasterFactory               from './toaster.factory';
import ToasterConfig4Factory        from './toaster.config4factory';


export default angular
    .module('chouette.toaster', [
        'ngNotify'
    ])
    .config(ToasterConfig4Factory)
    .factory('ToasterInterceptor', ToasterFactory)
    .name;
