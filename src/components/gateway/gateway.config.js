class GatewayConfig {

    /**
     * Initialize the routing
     */
    static initConfiguration($httpProvider) {
        $httpProvider.useLegacyPromiseExtensions = true;
        $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        //$httpProvider.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
        $httpProvider.defaults.xsrfCookieName = 'csrftoken';
        $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
    }
}

GatewayConfig.initConfiguration.$inject = ['$httpProvider'];

export default GatewayConfig.initConfiguration;
