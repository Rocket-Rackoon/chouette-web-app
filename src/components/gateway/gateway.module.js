import GatewayConfig        from './gateway.config';
import GatewayFactory       from './gateway.factory';
import GatewayAPIProvider   from './gateway.provider';


export default angular
    .module('chouette.gateway', [])
    .config(GatewayConfig)
    .factory('$gateway', GatewayFactory)
    .provider('GatewayAPIConfig', GatewayAPIProvider)
    .name;

