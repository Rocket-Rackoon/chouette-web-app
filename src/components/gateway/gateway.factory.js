/**
 * Store Gateway Factory
 */
class GatewayFactory {


    /**
     * Init Factory with dependency injection
     */
    static factory($http, GatewayAPIConfig, $q, LocalStorage) {
        return new GatewayFactory($http, GatewayAPIConfig, $q, LocalStorage);
    }

    /**
     * Constructor
     *
     * @param {$http}            $http
     * @param {GatewayAPIConfig} GatewayAPIConfig
     * @param $q
     * @param LocalStorage
     */
    constructor($http, GatewayAPIConfig, $q, LocalStorage) {

        let vm = this;
        vm.fetch = fetch;
        vm.push = push;
        vm.remove = remove;
        vm.query = query;

        function setDefaultCommonAuthorizationHeader() {
            let baseAuth = LocalStorage.get('auth');
            $http.defaults.headers.common.Authorization = "Basic " + baseAuth;
        }

        function generateCall(options, platform) {

            setDefaultCommonAuthorizationHeader();

            if ((!options.hasOwnProperty('url')) ||
                ((angular.isUndefined(options.url)) || (options.url === ''))) {
                throw new Error('You need to write url property in your object.');
            }

            // Activate $http secure with oauth.Js
            options.url = `${GatewayAPIConfig.$getConf[platform].url}${options.url}`;

            let deferred = $q.defer();

            $http(options)
            .then(
                (response) => deferred.resolve(response),
                (response) => deferred.reject(response)
            );

            return deferred.promise;
        }

        /**
         * permit to call the API with a GET
         * method and return a Promise.
         *
         * @param url
         * @param parameters
         * @returns {*}
         */
        function fetch (url, parameters) {
            return generateCall({
                url: url,
                method: 'GET',
                params: parameters || {}
            }, 'api');
        }


        /**
         * Permit to manage creation or
         * update a resource on your api
         *
         * @param options
         * @returns {*}
         */
        function push (options) {
            return generateCall(options, 'api');
        }


        /**
         * Permit to manage creation or
         * update a resource on your api
         *
         * @param url
         * @returns {*}
         */
        function remove (url) {
            return generateCall({
                url: url,
                method: 'DELETE'
            }, 'api');
        }


        /**
         * Permit to manage creation or
         * update a resource on your api
         *
         * @returns {*}
         * @param options
         * @param platform
         */
        function query (options, platform) {
            return generateCall(options, platform);
        }
    }


}

GatewayFactory.factory.$inject = ['$http', 'GatewayAPIConfig', '$q', 'LocalStorage'];

export default GatewayFactory.factory;
