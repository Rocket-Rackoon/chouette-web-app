/**
 * @ngDoc
 * @provider
 */
class GatewayAPIConfig {

    constructor() {

        let vm = this;

        vm.$get      = getProvider;
        vm.configure = setServers;


        /**
         *
         * @type {
         *  {
         *      api: {},
         *   }
         * }
         */
        let APIConf = {
            api: {}
        };


        /**
         * Setter new configuration
         *
         * @param {object} configuration
         */
        function setServers(configuration) {
            angular.extend (APIConf, configuration);
        }


        /**
         * Get the configuration provider
         *
         * @returns
         * {
         *  {
         *      $getConf: {
         *          api: {}
         *      }
         *   }
         * }
         */
        function getProvider() {
            return {
                $getConf: APIConf
            };
        }

    }

}

export default GatewayAPIConfig;
