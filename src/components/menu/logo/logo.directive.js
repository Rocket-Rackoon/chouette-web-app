class LogoDirective {

    static factory() {
        return new LogoDirective ();
    }

    /**
     * Constructor Directive GeoTargeting
     */
    constructor() {

        this.restrict = 'E';
        this.template = `
        <div hide-md>
            <img ng-src="img/logo-chouette.png" alt="logo chouette">
        </div>
        `;
    }
}

export default LogoDirective.factory;
