import LogoDirective from './logo.directive';

export default angular.module('chouette.logo', [])
    .directive('chouetteLogo', LogoDirective)
    .name;
