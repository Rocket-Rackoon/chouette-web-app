import NavModule        from './nav/nav.module';
import LogoModule       from './logo/logo.module';
// import PreNavModule     from './prenav/prenav.module';
import MenuController   from './menu.controller';
// import ProfileModule    from '../profile/profile.module';

require('./menu.scss');

export default angular
    .module('chouette.menu', [
            LogoModule,
            NavModule
        ]
        //[, , PreNavModule, ProfileModule]
    )
    .controller('MenuController', MenuController)
    .name;
