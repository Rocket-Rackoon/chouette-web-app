import PreNavController from './prenav.controller.js';

class PreNavDirective {

    static factory () {

        return new PreNavDirective();

    }

    /**
     * Constructor Directive GeoTargeting
     */
    constructor () {

        this.restrict = 'E';
        this.template = require('!!raw!./prenav.html');
        this.controller = PreNavController;
        this.controllerAs = 'prenavCtrl';
    }
}

export default PreNavDirective.factory;
