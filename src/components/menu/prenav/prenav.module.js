import PreNavDirective   from './prenav.directive';

require('./prenav.scss');

export default angular
    .module('power-app.prenav', [])
    .directive('pwPrenav', PreNavDirective)
    .name;
