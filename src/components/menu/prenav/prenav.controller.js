class PreNavController {

    /**
     * @param loginService
     * @param permissionService
     */
    constructor(loginService, permissionService) {
        'ngInject';

        let vm = this;

        vm.logout = loginService.logout;
        vm.authorizeType = permissionService.authorizeType;
    }
}

export default PreNavController;
