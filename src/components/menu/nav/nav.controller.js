// import {listNav}    from './nav.config';

class NavController {

    constructor($rootScope) {
        let vm = this;

        vm.islets = [];
        vm.plots = null;
        vm.analyzes = null;

        let updateIsletMenu = $rootScope.$on('ui.menu.ilots', (ev, data) => vm.islets = data);
        $rootScope.$on('$destroy', () => updateIsletMenu());

        vm.isletsIsEmpty = () => {
            return vm.islets.length === 0;
        };
        vm.isPlotIsEmpty = () => {
            if (vm.plots !== null)
                return vm.plots.length === 0;
        };
        vm.isAnalyzeIsEmpty = () => {
            if (vm.analyzes !== null)
                return vm.analyzes.length === 0;
        };

        vm.onSelectedIslet = (islet) => {
            vm.plots = islet.plot
        };

        vm.onSelectedPlot = (plot) => {
            vm.analyzes = plot.bilan_parcelle;
            $rootScope.$broadcast('ui.domain.plot', plot);
        };

        vm.onSelectedAnalyze = (/*analyze*/) => {
        }
    }
}

NavController.$inject = ['$rootScope'];

export default NavController;
