import NavDirective from './nav.directive';

export default angular
    .module ('chouette.nav', [])
    .directive ('chouetteNav', NavDirective)
    .name;
