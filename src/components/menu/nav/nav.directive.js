import NavController from './nav.controller.js';

class NavDirective {

    static factory() {
        return new NavDirective ();
    }

    /**
     * Constructor Directive GeoTargeting
     */
    constructor() {

        this.restrict     = 'E';
        this.template     = require ('!!raw-loader!./nav.html');
        this.controller   = NavController;
        this.controllerAs = 'navCtrl';
    }
}

export default NavDirective.factory;
