import 'angular-dropdowns';

export default angular
    .module ('chouette.dropdown', [
        'ngDropdowns'
    ])
    .name;
