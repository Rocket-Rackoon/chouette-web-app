export class Feature {

    constructor() {
        this.id = "";
        this.type = "Feature";
        this.properties = {};
        this.geometry = {};
    }
}
