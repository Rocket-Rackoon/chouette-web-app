import Moment from 'moment';

class LoginController {

    /**
     * constructor of Login Controller
     *
     * @param LocalStorage
     * @param LoginService
     * @param ngNotify
     */
    constructor(LocalStorage, LoginService, ngNotify) {
        let vm = this;

        vm.yearNow  = Moment ().year ();
        vm.onSubmit = submitLogin;

        /**
         * submit the credential
         *
         * @param credentials
         */
        function submitLogin(credentials) {
            let authentication = btoa (credentials.username + ':' + credentials.password);
            LocalStorage.set ('auth', authentication);
            LoginService
                .getUser ()
                .then (onLoginSuccess, onLoginError);
        }

        /**
         *
         * @param result
         */
        function onLoginSuccess(result) {
            LocalStorage.set('usr', angular.toJson(result.data));
            debugger;
        }

        function onLoginError() {
            LocalStorage.remove ('auth');
            ngNotify.set ("Les paramètres d'authentification sont incorrectes", 'error');
        }
    }

}

LoginController.$inject = ['LocalStorage', 'LoginService', 'ngNotify'];

export default LoginController;
