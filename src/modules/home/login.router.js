class HomeConfig {

    static initRoute ($stateProvider, $urlRouterProvider) {

        $stateProvider.state('login', {
            url: "/login",
            views:{
                'login': {
                    templateUrl: require("./login.tpl.html"),
                    controller: "LoginController as loginCtrl"
                }
            }
        });
        $urlRouterProvider.otherwise("/login");
    }
}

HomeConfig.initRoute.$inject = ['$stateProvider', '$urlRouterProvider'];

export default HomeConfig.initRoute;
