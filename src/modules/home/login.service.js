import StoreService from '../../store';

const RESOURCE = 'getUserData';

/**
 * Store Login Factory
 */
class LoginFactory extends StoreService {


    /**
     * Init Factory with dependency injection
     */
    static factory($gateway) {
        return new LoginFactory($gateway);
    }

    /**
     * Constructor
     *
     * @param $gateway
     */
    constructor($gateway) {

        super(`${RESOURCE}`, $gateway);

        let vm = this;
        vm.getUser = getUser;


        /**
         * get the user information
         * @return {*}
         */
        function getUser() {
            return $gateway.fetch(`${RESOURCE}/`);
        }

    }
}

LoginFactory.factory.$inject = ['$gateway'];

export default LoginFactory.factory;
