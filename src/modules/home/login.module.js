import angular          from 'angular';

import LoginController  from './login.controller';
import LoginRouter      from './login.router';
import LoginService     from './login.service';

require('./login.scss');

export default angular
    .module('chouette.login', [])
    .config(LoginRouter)
    .controller('LoginController', LoginController)
    .factory('LoginService', LoginService)
    .name;
