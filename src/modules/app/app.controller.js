class AppController {

    /**
     * Constructor class AppController
     *
     */
    constructor($state, LocalStorage) {

        let vm = this;
        // vm.granted = granted;
        refreshAccount();

        function refreshAccount() {
            let usr = LocalStorage.get('usr');

            if ((usr === null) &&
                (LocalStorage.get('auth') === null)) {
                $state.go('login');
            } else {
                vm.user = angular.fromJson(usr);
                $state.go('public.domain');
            }
        }
    }
}

AppController.$inject= ['$state', 'LocalStorage'];

export default AppController;
