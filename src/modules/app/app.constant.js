export const STATUSCOLOR = {
    NORMAL : {
        "color"      : "#555555",
        "weight"     : 0,
        "opacity"    : 0.85,
        "fillOpacity": 0.6
    },
    WARNING: {
        "color"      : "#F3C536",
        "weight"     : 2,
        "opacity"    : 0.85,
        "fillOpacity": 0.6
    },
    VALID   : {
        "color"      : "#94B758",
        "weight"     : 2,
        "opacity"    : 0.85,
        "fillOpacity": 0.6
    },
    ALERT  : {
        "color"      : "#FA7B58",
        "weight"     : 2,
        "opacity"    : 0.85,
        "fillOpacity": 0.6
    },
    CLEAN  : {
        "color"      : "#FFFFFF",
        "weight"     : 2,
        "opacity"    : 0.85,
        "fillOpacity": 0.1
    }
};
