
class AppConfig {

    static configure($stateProvider, $locationProvider, $httpProvider, GatewayAPIConfigProvider) {

        let parameters = require ('config/parameters.json');

        // $locationProvider.html5Mode(true).hashPrefix('!');

        $httpProvider.interceptors.push('httpInterceptor');
        GatewayAPIConfigProvider.configure (parameters);

        $stateProvider.state ('public', {
            url     : "/",
            abstract: true,
            views   : {
                // 'sidenav': {
                //     templateUrl: require("../../components/sidenav/sidenav.html"),
                //     controller: "SidenavController as sidenav"
                // },
                'menu': {
                    templateUrl: require ("../../components/menu/menu.tpl.html"),
                    controller : "MenuController as menuCtrl"
                }
            }
        });

        /**
         * Configure oauth token storage
         */
        /*    $tokenProvider.configure({
         storage: 'CookiesStorage',
         name: 'platform-token'
         });*/

    }
}

AppConfig.configure.$inject = ['$stateProvider', '$locationProvider', '$httpProvider', 'GatewayAPIConfigProvider'];

export default AppConfig.configure;
