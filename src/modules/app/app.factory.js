/**
 * App Factory
 */
class AppFactory {

    /**
     * @param $q
     * @param $injector
     */
    static factory($q, $injector) {
        return new AppFactory ($q, $injector);
    }

    constructor($q, $injector) {

        let vm = this;

        vm.responseError = responseError;


        function responseError(error) {

            if (error.status === 404) {
                //message.description = `The entity that you're looking for was not found.`;
            } else if (error.status === 401) {
                $injector.get('$state').go('login')
            } else if (error.status === 403) {
                //message.description = `The entity that you're looking for was not found.`;
            } else {
                //message.description = `Something wrong happened, please contact the support.`;
            }


            return $q.reject (error);
        }
    }
}

AppFactory.factory.$inject = ['$q', '$injector'];

export default AppFactory.factory;
