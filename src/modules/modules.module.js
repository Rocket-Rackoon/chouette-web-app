import DomainModule from './domain/domain.module';
import LoginModule  from './login/login.module';

export default angular
    .module('chouette.modules', [
        LoginModule,
        DomainModule,
    ])
    .name;
