class DomainConfig {

    static initRoute($stateProvider) {
        $stateProvider.state ('public.domain', {
            url    : "domaine",
            views  : {
                'content@': {
                    templateUrl: require ("./domain.tpl.html"),
                    controller : "DomainController as domainCtrl"
                }
            }
        });
    }
}

DomainConfig.initRoute.$inject = ['$stateProvider'];

export default DomainConfig.initRoute;

/*
 resolve: {
 ilotsAndParcelles: ['DomainService', function(DomainService) {
 return DomainService
 .getIlotsAndParcelles();
 }]
 },
 */
