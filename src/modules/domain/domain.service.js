import StoreService from '../../store';

const RESOURCE = 'getParcelleDataAndIlotsData';

/**
 * Store Domain Factory
 */
class DomainFactory extends StoreService {


    /**
     * Init Factory with dependency injection
     */
    static factory($gateway) {
        return new DomainFactory($gateway);
    }

    /**
     * Constructor
     *
     * @param $gateway
     */
    constructor($gateway) {

        super(`${RESOURCE}`, $gateway);


        /**
         * get the user information
         * @return {*}
         */
        this.getIlotsAndParcelles = () => {
            return $gateway.fetch(`${RESOURCE}/`);
        }

    }
}

DomainFactory.factory.$inject = ['$gateway'];

export default DomainFactory.factory;
