import DomainController from './domain.controller';
import DomainRouter     from './domain.router';
import DomainService    from './domain.service';

require ('./domain.scss');

export default angular
    .module ('chouette.domain', [])
    .config (DomainRouter)
    .controller ('DomainController', DomainController)
    .factory ('DomainService', DomainService)
    .name;
