/* global L */
import { Feature } from 'libs/classes/feature';
import { FeatureCollections } from 'libs/classes/featurecollections';

class DomainController {

    constructor(DomainService, $rootScope, leafletData, $scope, $log) {

        let vm                  = this;
        vm.leaflet              = {};
        let islands             = {};
        let listIslands         = [];
        let featuresCollections = null;

        init();

        /**
         * Initialize the domain controller
         * by settings Leaflet option and
         * download data from api
         */
        function init() {
            vm.leaflet.defaults = {
                scrollWheelZoom: false
            };

            featuresCollections = new FeatureCollections();

            vm.leaflet.center = {
                lat : 48.9577,
                lng : 3.2641,
                zoom: 13
            };

            vm.leaflet.events = {
                map: {
                    enable: ['zoomstart', 'drag', 'click', 'mousemove', 'mouseover'],
                    logic : 'emit'
                }
            };

            /*vm.leaflet.layers = {
             baselayers: {
             googleHybrid: {
             name         : 'Google Terrain',
             layerType    : 'SATELLITE',
             type         : 'google',
             maxNativeZoom: 18,
             maxZoom      : 25
             }
             }
             };*/


            DomainService
                .getIlotsAndParcelles()
                .then(
                    (response) => mappingParcelToIslet(response.data)
                );
        }

        /**
         * After receiving data from api,
         * clean the data and save it to listIsland
         * array.
         * Add a listener to update the menu when the
         * listIsland is up to date.
         *
         * @param data
         */
        const mappingParcelToIslet = (data) => {
            if (data.hasOwnProperty('ilot_info')) {
                data.ilot_info.map((islet) => {
                    islands[islet.id] = islet;
                    listIslands.push(islet);

                    if (data.hasOwnProperty('parcelle_info')) {
                        islands[islet.id].plot = data.parcelle_info.filter((plot) => {
                            if (plot.id_ilot === islet.id) {
                                mappingFeatureCollection(plot);
                                return plot;
                            }
                        });
                    }
                });
            }
            $rootScope.$broadcast('ui.menu.ilots', listIslands);
            $scope.$on("leafletDirectiveGeoJson.mouseover", function (ev, leafletPayload) {
                $log.debug(leafletPayload);
                //countryMouseover(leafletPayload.leafletObject.feature, leafletPayload.leafletEvent);
            });
            centerToMapJson();
        };


        /**
         * Mapping the each parcel info into a feature
         * to generate the FeatureCollection
         *
         * @param data
         */
        const mappingFeatureCollection = (data) => {
            let feature        = new Feature();
            feature.id         = data.id;
            feature.geometry   = data.geometry;
            feature.properties = {
                id         : data.id,
                islet_id   : data.id_ilot,
                name       : data.name,
                bilan      : data.bilan_parcelle,
                background : data.background_map,
                dateUpdated: data.date_last_photoshooting
            };

            featuresCollections.features.push(feature);
            let geoJson        = angular.toJson(featuresCollections);
            vm.leaflet.geojson = {
                data         : angular.fromJson(geoJson),
                onEachFeature: updateFeatureAfterMapping
            };
        };


        /**
         *
         *
         * @param feature
         * @param layer
         */
        const updateFeatureAfterMapping = (feature, layer) => {
            layer.bindPopup(`feature.properties.name <br/> ${generateLinkOnPopup(feature)}`);
            layer.on('click', function () {
                $log.warn(layer.on);

            });
        };


        /**
         * Generate a list of link on popup
         *
         * @param feature
         * @return {string}
         */
        const generateLinkOnPopup = (feature) => {
            let html = '';
            if (feature.properties.bilan.length > 0) {
                feature.properties.bilan.map((resume) => {
                    if ((resume.max_detected_value > resume.threshold_alert) && resume.criticality > 1) {
                        html += `<a href='#/parcelle/${feature.id}/${resume.category_item_detected}/${resume.item_detected}'>
                            ${feature.name} - ${resume.item_detected}
                         </a>
                         <br/>`;
                    }
                });
            }

            return html;
        };

        /**
         * center map with the GeoJson
         */
        const centerToMapJson = () => {
            leafletData
                .getMap()
                .then(
                    (map) => {
                        let latlngs     = [];
                        let coordinates = vm.leaflet.geojson.data.features[0].geometry.coordinates;
                        for (let i in coordinates) {
                            if (coordinates.hasOwnProperty(i)) {
                                let _coordinate = coordinates[i];
                                for (let j in _coordinate) {
                                    if (_coordinate.hasOwnProperty(j)) {
                                        let points = _coordinate[j];
                                        for (let k in points) {
                                            if (points.hasOwnProperty(k)) {
                                                latlngs.push(L.GeoJSON.coordsToLatLng(points[k]));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        map.fitBounds(latlngs);
                    }
                );
        };

        const selectPlotEvt = $rootScope.$on('ui.domain.plot', (evt, data) => {
            vm.leaflet.geojson = data.geometry;
        });
        $rootScope.$on('$destroy', () => selectPlotEvt());
    }

}

DomainController.$inject = ['DomainService', '$rootScope', 'leafletData', '$scope', '$log'];

export default DomainController;
