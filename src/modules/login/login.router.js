class HomeConfig {

    static initRoute ($stateProvider) {

        $stateProvider.state('login', {
            url: "/login",
            views:{
                'login': {
                    templateUrl: require("./login.tpl.html"),
                    controller: "LoginController as loginCtrl"
                }
            }
        });
    }
}

HomeConfig.initRoute.$inject = ['$stateProvider'];

export default HomeConfig.initRoute;
