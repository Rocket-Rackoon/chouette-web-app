import Moment from 'moment';

class LoginController {

    /**
     * constructor of Login Controller
     *
     * @param LocalStorage
     * @param LoginService
     * @param ngNotify
     * @param $state
     * @param $timeout
     * @param $parent
     */
    constructor(LocalStorage, LoginService, ngNotify, $state, $timeout, $scope) {
        let vm = this;

        vm.yearNow  = Moment ().year ();
        vm.onSubmit = submitLogin;

        /**
         * submit the credential
         *
         * @param credentials
         */
        function submitLogin(credentials) {
            let authentication = btoa (credentials.username + ':' + credentials.password);
            LocalStorage.set ('auth', authentication);
            LoginService
                .getUser ()
                .then (onLoginSuccess, onLoginError);
        }

        /**
         *
         * @param result
         */
        function onLoginSuccess(result) {
            LocalStorage.set('usr', angular.toJson(result.data));
            $scope.$parent.appCtrl.user = result.data;
            $timeout(() => $state.go('public.domain'), 300);
        }

        function onLoginError() {
            LocalStorage.remove ('auth');
            ngNotify.set ("Les paramètres d'authentification sont incorrectes", 'error');
        }
    }

}

LoginController.$inject = ['LocalStorage', 'LoginService', 'ngNotify', '$state', '$timeout', '$scope'];

export default LoginController;
