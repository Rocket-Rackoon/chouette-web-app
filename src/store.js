/**
 * Store Service
 */
class StoreService {

    /**
     * Constructor
     */
    constructor (resource, $gateway) {

        if ((angular.isUndefined(resource)) ||
            (resource === '')) {

            throw new Error('The resource must be defined');
        }

        let vm = this;

        vm.getRessources = getResources;
        vm.getRessourceById = getResourceById;
        vm.save = save;


        function getResources() {
            return $gateway.fetch(resource + '/');
        }


        function getResourceById(id) {
            return $gateway.fetch(resource + '/' + id + '/');
        }


        function save (resource) {
            return $gateway.push(resource + '/');
        }
    }
}

export default StoreService;
