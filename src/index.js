// import all images
import './images';

// import SASS
import './public/scss/chouette.scss';

// import angular dependencies
import angular          from 'angular';
import ngAria           from 'angular-aria';
import ngAnimate        from 'angular-animate';
import ngTranslate      from 'angular-translate';
import ngSanitize       from 'angular-sanitize';
import ngMessages       from 'angular-messages';

// Remove this line below when you are in production
import 'angular-mocks';
import backendDev from './backendDev';

import ngUiRouter       from 'angular-ui-router';


import AppStatus        from 'modules/app/app.constant';
import AppConfig        from 'modules/app/app.config';
import AppController    from 'modules/app/app.controller';
import AppInterceptor   from 'modules/app/app.factory';

import modules          from 'modules/modules.module';
import components       from 'components/components.module';

export default angular
    .module('chouette.app', [
        //angular dependencies
        ngUiRouter,
        ngAnimate,
        ngAria,
        ngTranslate,
        ngSanitize,
        ngMessages,

        'ngMockE2E', // remove it in production

        // Our components
        components,

        // Our modules
        modules
    ])
    .config (AppConfig)
    .constant('STATUSCOLOR', AppStatus)
    .factory('httpInterceptor', AppInterceptor)
    .controller ('AppController', AppController)
    .run(backendDev) // remove it in production
    //.run (MainRunning);
